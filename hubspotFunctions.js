const axios = require('axios');
const dotenv = require('dotenv').config();
const functions = require('./functions');

const accessToken = process.env.hubspot_access_token;

const hubspot = require('@hubspot/api-client')
const hubspotClient = new hubspot.Client({ accessToken: accessToken })

const domain = 'https://api.hubapi.com/crm/v3/objects';

//getAllProductionIdsByCategories
async function matchDealsAndContacts(req, res){
    console.log("Start method matchDealsAndContacts");

    let allDeals = await hubspotClient.crm.deals.getAll();
    
    let result = {};
    let totalObjects = [];

    //for(let i=0; i<285; i++){
    for(let i=0; i<allDeals.length; i++){
        let deal = allDeals[i];

        let pipeline = deal.properties.pipeline;
        // other pipeline|| pipeline === '43019739'
        if(pipeline === 'default' || pipeline === '43019739'){
            //getInfoFromDeal
            let dealById = await hubspotClient.crm.deals.basicApi.getById(deal.id, [
                'pipeline', 'id_app', 'dealname', 'dealstage', 'hubspot_owner_id'
            ], undefined, ['contacts']);
            
            if(undefined !== dealById && undefined !== dealById.properties && dealById.properties.id_app !== null){
                let associations = dealById.associations;
                let element = {};

                let dealId = deal.id;
                element.dealId = dealId;
                element.dealName = dealById.properties.dealname;
                element.dealpipeline = dealById.properties.pipeline;
                element.dealstage = dealById.properties.dealstage;
                element.idApp = dealById.properties.id_app;
                element.associations = associations;

                //si ya existen associations a contacts, no hacemos nada
                if(undefined !== associations){
                    console.log("Ya hay association ");
                    element.associationExists = "YES";

                //si NO existen, vamos a ver si la podemos crear
                }else{
                    element.associationExists = "NO";

                    let id_app = dealById.properties.id_app;
                    element.idApp = id_app;

                    if(id_app !== "**" && id_app !== "***"){
                        let publicObjectSearchRequest = {
                            filterGroups: [{
                                filters: [{
                                    "operator": "CONTAINS_TOKEN",
                                    "propertyName": "superadmin_link",
                                    "value": id_app
                                }]
                            }], 
                            properties: ["superadmin_link", "email", "company"]
                        };

                        let contactsMatched = await hubspotClient.crm.contacts.searchApi.doSearch(publicObjectSearchRequest);
                        if(undefined !== contactsMatched && null !== contactsMatched){
                            if(contactsMatched.total > 0){
                                let contactsMatchedResult = contactsMatched.results;
                                for(let c=0; c<contactsMatchedResult.length; c++){
                                    let contactMatched = contactsMatchedResult[c];
                                    let superadminLink = contactMatched.properties.superadmin_link;
                                    if(superadminLink.includes('getritmo.com/admin/companies/')){
                                        let linkAux = superadminLink.split("/companies/")[1];
                                        let contactIdApp = linkAux.split("/")[1];
                                        console.log("Esto es id_app del deal: " + id_app);

                                        if(contactIdApp === id_app){
                                            console.log("---------  Tenemos un match: ------" + superadminLink);
                                            //crea association
                                            element.match = "YES";

                                            let contactId = contactMatched.id;
                                            element.contactId = contactId;
                                            element.contactEmail = contactMatched.properties.email;
                                            element.contactCompany = contactMatched.properties.company;
                                            element.link = superadminLink;

                                            let BatchInputPublicAssociation = { inputs: [{"_from":{"id":dealId},"to":{"id":contactId},"type":"deal_to_contact"}] };
                                            const fromObjectType = "deal";
                                            const toObjectType = "contact";

                                            try {
                                                const apiResponse = await hubspotClient.crm.associations.batchApi.create(fromObjectType, toObjectType, BatchInputPublicAssociation);
                                                //console.log(JSON.stringify(apiResponse.body, null, 2));
                                            } catch (e) {
                                                e.message === 'HTTP request failed'
                                                  ? console.error(JSON.stringify(e.response, null, 2))
                                                  : console.error(e)
                                            }

                                        }else{
                                            console.log("Aqui no: " + superadminLink)
                                        }
                                    }else{
                                        console.log("Super Admin Link distinto: " + superadminLink);
                                    }
                                }
                            }else{
                                console.log("Error en contactsMatched. Total = 0: " + contactsMatched.result + " valores: " + contactsMatched.result);
                                element.match = "NO";
                            }
                        }else{
                            console.log("Error en contactsMatched: " + contactsMatched.result);
                            element.match = "NO";
                        }
                    }else{
                        element.match = "NO";
                    }
                }
                totalObjects.push(element);
            }
        }
    }/*else{
            console.log("The deal " + deal.id + " is not in our pipelines");
    }*/

    console.log("---- Finishing -----");

    functions.convertJsontoCsv(totalObjects, "fileFinal-method01-01");

    result.numTotal = totalObjects.length;
    result.objects = totalObjects;
    
    res.status(200).send(result);
}

async function matchDealsAndContactsByDealnameAndEmail(req, res){
    console.log("Start method matchDealsAndContactsByDealnameAndEmail");

    let allDeals = await hubspotClient.crm.deals.getAll();
    
    let result = {};
    let totalObjects = [];

    //for(let i=0; i<400; i++){
    for(let i=0; i<allDeals.length; i++){
        let deal = allDeals[i];

        let pipeline = deal.properties.pipeline;
        // other pipeline|| pipeline === '43019739'
        if(pipeline === 'default' || pipeline === '43019739'){
            //getInfoFromDeal
            let dealById = await hubspotClient.crm.deals.basicApi.getById(deal.id, [
                'pipeline', 'dealname', 'dealstage', 'hubspot_owner_id'
            ], undefined, ['contacts']);
            
            if(undefined !== dealById && undefined !== dealById.properties){
                let associations = dealById.associations;
                let dealId = deal.id;

                //si ya existen associations a contacts, no hacemos nada
                if(undefined !== associations){
                    console.log("Ya hay association ");
                    let element = {};

                    element.dealId = dealId;
                    element.dealName = dealById.properties.dealname;
                    element.dealpipeline = dealById.properties.pipeline;
                    element.dealstage = dealById.properties.dealstage;
                    element.associationExists = "YES";

                    totalObjects.push(element);

                //si NO existen, vamos a ver si la podemos crear, buscando por dealname y domain
                }else{
                    let dealName = dealById.properties.dealname;
                    if(dealName !== undefined){
                        let dealNameFormatted = dealName.split(" - ")[0].toLowerCase().trim();
                        dealNameFormatted = dealNameFormatted
                        .replaceAll("s.l.u.", "").replaceAll("s.l.u", "").replaceAll("slu", "")
                        .replaceAll("s.l.", "").replaceAll("s.l", "").replaceAll("sl", "")
                        .replaceAll("ltd", "");
                        
                        let auxName = "";
                        if(dealNameFormatted.includes("(") === true && dealNameFormatted.includes("oliver") === false){
                            auxName = dealNameFormatted.split("(")[1].replace(")", "").replaceAll(".", "").replaceAll(",", "");
                            dealNameFormatted = dealNameFormatted.split("(")[0];
                        }
                        if(dealNameFormatted.includes("/") === true && dealNameFormatted.includes("oliver") === false){
                            dealNameFormatted = dealNameFormatted.split("/")[0];
                        }

                        let filters = [];
                        if(auxName !== ""){
                            filters = [{
                                filters: [{
                                    "operator": "CONTAINS_TOKEN",
                                    "propertyName": "email",
                                    "value": dealNameFormatted
                                }],
                                filters: [{
                                    "operator": "CONTAINS_TOKEN",
                                    "propertyName": "email",
                                    "value": auxName
                                }],
                            }]
                        }else{
                            filters = [{
                                filters: [{
                                    "operator": "CONTAINS_TOKEN",
                                    "propertyName": "email",
                                    "value": dealNameFormatted
                                }],
                            }]
                        }

                        let publicObjectSearchRequest = {
                            filterGroups: filters,
                            properties: ["email", "company"]
                        };

                        let contactsMatched = await hubspotClient.crm.contacts.searchApi.doSearch(publicObjectSearchRequest);
                        if(undefined !== contactsMatched && null !== contactsMatched){
                            if(contactsMatched.total > 0){
                                let contactsMatchedResult = contactsMatched.results;
                                for(let c=0; c<contactsMatchedResult.length; c++){
                                    let contactMatched = contactsMatchedResult[c];
                                    let contactEmail = contactMatched.properties.email;
                                    console.log("---------  Tenemos un match: ------" + contactEmail);

                                    let element = {};

                                    element.dealId = dealId;
                                    element.dealName = dealById.properties.dealname;
                                    element.dealpipeline = dealById.properties.pipeline;
                                    element.dealstage = dealById.properties.dealstage;
                                    element.associationExists = "NO";
                                    element.dealNameFormatted = dealNameFormatted;
                                    element.match = "YES";
                                    element.numMatches = contactsMatchedResult.length;

                                    //crea association
                                    let contactId = contactMatched.id;
                                    element.contactId = contactId;
                                    element.contactEmail = contactMatched.properties.email;
                                    element.contactCompany = contactMatched.properties.company;

                                    totalObjects.push(element);

                                    let BatchInputPublicAssociation = { inputs: [{"_from":{"id":dealId},"to":{"id":contactId},"type":"deal_to_contact"}] };
                                    const fromObjectType = "deal";
                                    const toObjectType = "contact";

                                    try {
                                        const apiResponse = await hubspotClient.crm.associations.batchApi.create(fromObjectType, toObjectType, BatchInputPublicAssociation);
                                        //console.log(JSON.stringify(apiResponse.body, null, 2));
                                    } catch (e) {
                                        e.message === 'HTTP request failed'
                                            ? console.error(JSON.stringify(e.response, null, 2))
                                            : console.error(e)
                                    }
                                }
                            }else{
                                console.log("Error en contactsMatched. Total = 0: ");

                                console.log("Looking for dealNameFormatted: " + dealNameFormatted + " && auxName: " + auxName);
                                let element = {};
                                element.dealId = dealId;
                                element.dealName = dealById.properties.dealname;
                                element.dealpipeline = dealById.properties.pipeline;
                                element.dealstage = dealById.properties.dealstage;
                                element.associationExists = "NO";
                                element.dealNameFormatted = dealNameFormatted;
                                element.match = "NO";

                                totalObjects.push(element);
                            }
                        }else{
                            console.log("Error en contactsMatched: " + contactsMatched.result);
                            element.match = "NO";
                        }
                    }else{
                        element.match = "NO";
                        console.log("El nombre del deal no existe.")
                    }
                }
                
            }
        }/*else{
            console.log("The deal " + deal.id + " is not in our pipelines");
        }*/
    }

    console.log("---- Finishing -----");

    functions.convertJsontoCsv(totalObjects, "file-method02_conAsociaciones_02");

    result.numTotal = totalObjects.length;
    result.objects = totalObjects;
    
    res.status(200).send(result);
}

async function matchDealsAndCompaniesByDealnameAndEmail(req, res){
    console.log("Start method matchDealsAndCompaniesByDealnameAndEmail");

    let allDeals = await hubspotClient.crm.deals.getAll();
    
    let result = {};
    let totalObjects = [];

    //for(let i=0; i<400; i++){
    for(let i=0; i<allDeals.length; i++){
        let deal = allDeals[i];

        let pipeline = deal.properties.pipeline;
        // other pipeline|| pipeline === '43019739'
        if(pipeline === 'default' || pipeline === '43019739'){
            //getInfoFromDeal
            let dealById = await hubspotClient.crm.deals.basicApi.getById(deal.id, [
                'pipeline', 'dealname', 'dealstage', 'hubspot_owner_id'
            ], undefined, ['companies']);
            
            if(undefined !== dealById && undefined !== dealById.properties){
                let associations = dealById.associations;
                let dealId = deal.id;

                //si ya existen associations a contacts, no hacemos nada
                if(undefined !== associations){
                    console.log("Ya hay association ");
                    let element = {};

                    element.dealId = dealId;
                    element.dealName = dealById.properties.dealname;
                    element.dealpipeline = dealById.properties.pipeline;
                    element.dealstage = dealById.properties.dealstage;
                    element.associationExists = "YES";

                    totalObjects.push(element);

                //si NO existen, vamos a ver si la podemos crear, buscando por dealname y domain
                }else{
                    let dealName = dealById.properties.dealname;
                    if(dealName !== undefined){
                        let dealNameFormatted = dealName.split(" - ")[0].toLowerCase().trim();
                        dealNameFormatted = dealNameFormatted
                        .replaceAll("s.l.u.", "").replaceAll("s.l.u", "").replaceAll("slu", "")
                        .replaceAll("s.l.", "").replaceAll("s.l", "").replaceAll("sl", "")
                        .replaceAll("ltd", "");

                        let auxName = "";
                        if(dealNameFormatted.includes("(") === true && dealNameFormatted.includes("oliver") === false){
                            auxName = dealNameFormatted.split("(")[1].replace(")", "").replaceAll(".", "").replaceAll(",", "");
                            dealNameFormatted = dealNameFormatted.split("(")[0];
                        }
                        if(dealNameFormatted.includes("/") === true && dealNameFormatted.includes("oliver") === false){
                            dealNameFormatted = dealNameFormatted.split("/")[0];
                        }

                        //define filters
                        let filters = [];
                        if(auxName !== ""){
                            filters = [{
                                filters: [{
                                    "operator": "CONTAINS_TOKEN",
                                    "propertyName": "name",
                                    "value": dealNameFormatted
                                }],
                                filters: [{
                                    "operator": "CONTAINS_TOKEN",
                                    "propertyName": "domain",
                                    "value": dealNameFormatted
                                }],
                                filters: [{
                                    "operator": "CONTAINS_TOKEN",
                                    "propertyName": "name",
                                    "value": auxName
                                }],
                                filters: [{
                                    "operator": "CONTAINS_TOKEN",
                                    "propertyName": "domain",
                                    "value": auxName
                                }],
                            }]
                        }else{
                            filters = [{
                                filters: [{
                                    "operator": "CONTAINS_TOKEN",
                                    "propertyName": "name",
                                    "value": dealNameFormatted
                                }],
                                filters: [{
                                    "operator": "CONTAINS_TOKEN",
                                    "propertyName": "domain",
                                    "value": dealNameFormatted
                                }],
                            }]
                        }

                        let publicObjectSearchRequest = {
                            filterGroups: filters,
                            properties: ["name", "domain"]
                        };

                        /*let publicObjectSearchRequest = {
                            filterGroups: [{
                                filters: [{
                                    "operator": "CONTAINS_TOKEN",
                                    "propertyName": "name",
                                    "value": dealNameFormatted
                                }],
                                filters: [{
                                    "operator": "CONTAINS_TOKEN",
                                    "propertyName": "domain",
                                    "value": dealNameFormatted
                                }],
                            }], 
                            properties: ["name", "domain"]
                        };*/

                        let companiesMatched = await hubspotClient.crm.companies.searchApi.doSearch(publicObjectSearchRequest);
                        if(undefined !== companiesMatched && null !== companiesMatched){
                            if(companiesMatched.total > 0){
                                let contactsMatchedResult = companiesMatched.results;
                                for(let c=0; c<contactsMatchedResult.length; c++){
                                    let contactMatched = contactsMatchedResult[c];
                                    let companyName = contactMatched.properties.name;
                                    console.log("---------  Tenemos un match: ------" + companyName);

                                    let element = {};

                                    element.dealId = dealId;
                                    element.dealName = dealById.properties.dealname;
                                    element.dealpipeline = dealById.properties.pipeline;
                                    element.dealstage = dealById.properties.dealstage;
                                    element.associationExists = "NO";
                                    element.dealNameFormatted = dealNameFormatted;
                                    element.match = "YES";
                                    element.numMatches = contactsMatchedResult.length;

                                    //crea association
                                    let companyId = contactMatched.id;
                                    element.companyId = companyId;
                                    element.name = contactMatched.properties.name;
                                    element.domain = contactMatched.properties.domain;

                                    totalObjects.push(element);

                                    let BatchInputPublicAssociation = { inputs: [{"_from":{"id":dealId},"to":{"id":companyId},"type":"deal_to_company"}] };
                                    const fromObjectType = "deal";
                                    const toObjectType = "company";

                                    try {
                                        const apiResponse = await hubspotClient.crm.associations.batchApi.create(fromObjectType, toObjectType, BatchInputPublicAssociation);
                                        //console.log(JSON.stringify(apiResponse.body, null, 2));
                                    } catch (e) {
                                        e.message === 'HTTP request failed'
                                            ? console.error(JSON.stringify(e.response, null, 2))
                                            : console.error(e)
                                    }
                                }
                            }else{
                                console.log("Error en companiesMatched. Total = 0: " + companiesMatched.result);
                                let element = {};
                                element.dealId = dealId;
                                element.dealName = dealById.properties.dealname;
                                element.dealpipeline = dealById.properties.pipeline;
                                element.dealstage = dealById.properties.dealstage;
                                element.associationExists = "NO";
                                element.dealNameFormatted = dealNameFormatted;
                                element.match = "NO";

                                totalObjects.push(element);
                            }
                        }else{
                            console.log("Error en companiesMatched: " + companiesMatched.result);
                            element.match = "NO";
                        }
                    }else{
                        element.match = "NO";
                        console.log("El nombre del deal no existe.")
                    }
                }
                
            }
        }/*else{
            console.log("The deal " + deal.id + " is not in our pipelines");
        }*/
    }

    console.log("---- Finishing -----");

    functions.convertJsontoCsv(totalObjects, "dealsCompanies-method01");

    result.numTotal = totalObjects.length;
    result.objects = totalObjects;
    
    res.status(200).send(result);
}

module.exports = {
    matchDealsAndContacts,
    matchDealsAndContactsByDealnameAndEmail,
    matchDealsAndCompaniesByDealnameAndEmail,
}