const converter = require('json-2-csv');
const fs = require('fs');
const { parse } = require("csv-parse");

function convertJsontoCsv(values, fileName){

    converter.json2csv(values, (err, csv) => {
        if (err) {
          throw err
        }

        fs.writeFileSync("csvs/" + fileName + ".csv", csv);
    });
    
}

function convertJsontoFile(values, fileName){
    fs.writeFileSync("jsons/" + fileName + ".json", values);
}

module.exports = {
    convertJsontoCsv,
    convertJsontoFile,
}