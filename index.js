const express = require('express');
const app = express();
const hubspotFunctions = require('./hubspotFunctions');

app.listen(
    process.env.PORT,
    () => console.log('We are live')
);

app.get('/ok', (req, res) => {
    res.status(200).send({
        key: 200,
        value: 'OK'
    })
});

app.get('/hubspot/matchDealsAndContacts/', (req, res) => {
    hubspotFunctions.matchDealsAndContacts(req, res);
});

app.get('/hubspot/matchDealsAndContactsByDealnameAndEmail/', (req, res) => {
    hubspotFunctions.matchDealsAndContactsByDealnameAndEmail(req, res);
});

app.get('/hubspot/matchDealsAndCompaniesByDealnameAndEmail/', (req, res) => {
    hubspotFunctions.matchDealsAndCompaniesByDealnameAndEmail(req, res);
});